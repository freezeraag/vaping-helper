package de.freezeraag.vapinghelper;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;
import android.util.LongSparseArray;

import java.util.HashMap;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

/**
 * Created by Michael Lorenz
 * 04.05.2016.
 */
public class NotificationPublisher extends BroadcastReceiver {

    private static NotificationPublisher instance = null;
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    private static AlarmManager alarmManager = null;
    private static HashMap<String, LongSparseArray<PendingIntent>> alarms;
    private static SQLiteDatabase read;
    private static String tableNameSettings;
    private static String[] tableSettingsColumns;
    private static String[] tableSettingsKeys;

    public static void cancelSteeping(String recipe, long startTime) {
        PendingIntent toBeDeleted = alarms.get(recipe).get(startTime);
        alarms.get(recipe).remove(startTime);
        if (alarms.get(recipe).size() == 0) {
            alarms.remove(recipe);
        }
        alarmManager.cancel(toBeDeleted);
    }

    public static void cancelAllSteepings() {
        if (alarms != null) {
            for (String recipe : alarms.keySet()) {
                for (int i = 0; i < alarms.get(recipe).size(); i++) {
                    alarmManager.cancel(alarms.get(recipe).valueAt(i));
                }
            }
            alarms.clear();
        }
    }

    public static NotificationPublisher getInstance(Context context) {
        if (instance == null) {
            instance = new NotificationPublisher();
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarms = new HashMap<>();
            VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(context);
            read = db.getWritableDatabase();
            tableNameSettings = context.getResources().getString(R.string.settings_table_name);
            tableSettingsColumns = context.getResources().getStringArray(R.array.settings_table_columns);
            tableSettingsKeys = context.getResources().getStringArray(R.array.settings_keys);
        }
        return instance;
    }

    public AlarmManager getAlarmManager() {
        return alarmManager;
    }

    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);

        notificationManager.notify(id, notification);
    }

    public void addNotification(Context context, String recipe, long startTime, float duration, String measure) {
        Cursor cursor = read.query(tableNameSettings, tableSettingsColumns, tableSettingsColumns[0] + "=?",
                new String[]{tableSettingsKeys[0]}, null, null, null);
        cursor.moveToNext();
        if (cursor.getString(1).equals("true")) {
            int factor = 1000;
            if (measure.equals("days")) {
                factor *= 24 * 60 * 60;
            } else if (measure.equals("hours")) {
                factor *= 60 * 60;
            }
            long durationInMillis = (long) (duration * factor);
            long endingPoint = startTime + durationInMillis;
            long difference = endingPoint - System.currentTimeMillis();
            if (difference > 0) {
                // register the notification alarm
                scheduleNotification(context, getNotification(context, recipe), difference, recipe, startTime);
            } else {
                // push notification if duration elapsed during reboot
                scheduleNotification(context, getNotification(context, recipe), 0, recipe, startTime);
            }
        }
        cursor.close();
    }

    private void scheduleNotification(Context context, Notification notification, long delay, String recipe, long startTime) {
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        getAlarmManager().set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        if (!alarms.containsKey(recipe)) {
            alarms.put(recipe, new LongSparseArray<PendingIntent>());
        }
        alarms.get(recipe).append(startTime, pendingIntent);
    }

    private Notification getNotification(Context context, String content) {
        Notification.Builder builder = new Notification.Builder(context);
        Intent resultIntent = new Intent(context, Steepings.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        builder.setContentTitle("Ein Liquid ist fertig gereift");
        builder.setContentText(content);
        builder.setSmallIcon(R.drawable.ic_invert_colors_white_24dp);
        builder.setAutoCancel(true);
        Notification notification = builder.build();
        Cursor cursor = read.query(tableNameSettings, tableSettingsColumns, tableSettingsColumns[0] + "=?",
                new String[]{tableSettingsKeys[1]}, null, null, null);
        cursor.moveToNext();
        if (cursor.getString(1).equals("true")) {
            notification.defaults = Notification.DEFAULT_VIBRATE;
        }
        cursor.close();
        return notification;
    }
}
