package de.freezeraag.vapinghelper;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class Flavors extends AppCompatActivity {

    private SQLiteDatabase write;
    private String tableNameFlavor;
    private String[] tableFlavorColumns;
    private String tableNameFlavorInRecipe;
    private String[] tableFlavorInRecipeColumns;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);

        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        deleteBtn.setEnabled(true).setVisible(true);

        MenuItem addBtn = menu.findItem(R.id.menu_add);
        addBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_add:
                newFlavor();
                break;
            case R.id.menu_delete:
                clearEntries();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(Flavors.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent reload = getIntent();
        startActivity(reload);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flavors);
        this.setTitle(getResources().getString(R.string.flavors_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        final SQLiteDatabase read = db.getReadableDatabase();
        write = db.getWritableDatabase();
        tableNameFlavor = getResources().getString(R.string.flavors_table_name);
        tableFlavorColumns = getResources().getStringArray(R.array.flavors_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        tableFlavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);
        Cursor cursor = read.query(tableNameFlavor, tableFlavorColumns, null, null, null, null,
                tableFlavorColumns[0] + " ASC", null);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.myFlavors);

        while (cursor.moveToNext()) {
            final String manufacturer = cursor.getString(0);
            final String flavorName = cursor.getString(1);
            final String flavorPrice = String.valueOf(cursor.getFloat(2));

            // make a new card
            CardView card = (CardView) getLayoutInflater().inflate(R.layout.list_element_layout, linearLayout, false);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Flavors.this, CreateFlavor.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(tableFlavorColumns[0], manufacturer);
                    bundle.putString(tableFlavorColumns[1], flavorName);
                    bundle.putString(tableFlavorColumns[2], flavorPrice);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            };

            View.OnLongClickListener longClickListener = new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Flavors.this);
                    deleteDialog.setMessage(R.string.dialog_clear_flavor_message);
                    deleteDialog.setCancelable(true);

                    deleteDialog.setPositiveButton(
                            R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // cursor for flavor_in_recipe_table
                                    final Cursor cursorFlavorInRecipe = read.query(tableNameFlavorInRecipe,
                                            tableFlavorInRecipeColumns,
                                            tableFlavorInRecipeColumns[1] + "=? AND " +
                                                    tableFlavorInRecipeColumns[2] + "=?",
                                            new String[]{manufacturer, flavorName}, null, null, null, null);

                                    if (cursorFlavorInRecipe.getCount() > 0) {
                                        dialog.cancel();
                                        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Flavors.this);
                                        String message = getResources().getString(R.string.dialog_clear_flavor_found_in_recipe1) + " " +
                                                cursorFlavorInRecipe.getCount() + " " + getResources().getString(R.string.dialog_clear_flavor_found_in_recipe2) +
                                                " " + getResources().getString(R.string.dialog_clear_flavor_found_in_recipe3);
                                        deleteDialog.setMessage(message);
                                        deleteDialog.setCancelable(true);

                                        deleteDialog.setPositiveButton(
                                                R.string.yes,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        cursorFlavorInRecipe.close();
                                                        write.delete(tableNameFlavorInRecipe,
                                                                tableFlavorInRecipeColumns[1] + "=? AND " +
                                                                        tableFlavorInRecipeColumns[2] + "=?",
                                                                new String[]{manufacturer, flavorName});
                                                        write.delete(tableNameFlavor, tableFlavorColumns[0] +
                                                                        "=? AND " + tableFlavorColumns[1] + "=?",
                                                                new String[]{manufacturer, flavorName});
                                                        dialog.cancel();
                                                        Toast.makeText(Flavors.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                                        finish();
                                                    }
                                                }
                                        );

                                        deleteDialog.setNegativeButton(
                                                R.string.no,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                }
                                        );

                                        AlertDialog alert = deleteDialog.create();
                                        alert.show();
                                    } else {
                                        write.delete(tableNameFlavor, tableFlavorColumns[0] +
                                                        "=? AND " + tableFlavorColumns[1] + "=?",
                                                new String[]{manufacturer, flavorName});
                                        dialog.cancel();
                                        Toast.makeText(Flavors.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            }
                    );

                    deleteDialog.setNegativeButton(
                            R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );

                    AlertDialog alert = deleteDialog.create();
                    alert.show();
                    return true;
                }
            };
            Button aFlavor = (Button) card.findViewById(R.id.list_element_btn);
            String btnText = manufacturer.charAt(0) + "" + flavorName.charAt(0);
            aFlavor.setText(btnText);
            aFlavor.setOnClickListener(listener);
            aFlavor.setOnLongClickListener(longClickListener);

            TextView textView = (TextView) card.findViewById(R.id.list_element_textView);
            String header = manufacturer + "\n" + flavorName;
            textView.setText(header);
            card.setOnClickListener(listener);
            card.setOnLongClickListener(longClickListener);

            if (linearLayout != null) {
                linearLayout.addView(card);
            }
        }
        cursor.close();
    }

    private void newFlavor() {
        Intent openNewFlavor = new Intent(this, CreateFlavor.class);
        startActivity(openNewFlavor);
    }

    private void clearEntries() {
        AlertDialog.Builder deleteDbDialog = new AlertDialog.Builder(Flavors.this);
        deleteDbDialog.setMessage(R.string.dialog_clear_table_flavors_message);
        deleteDbDialog.setCancelable(true);

        deleteDbDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        write.delete(tableNameFlavor, null, null);
                        write.delete(getString(R.string.flavor_in_recipes_table_name), null, null);
                        Intent reload = getIntent();
                        startActivity(reload);
                        dialog.cancel();
                        finish();
                    }
                }
        );

        deleteDbDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDbDialog.create();
        alert.show();
    }
}
