package de.freezeraag.vapinghelper;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class CreateBase extends AppCompatActivity {

    private SQLiteDatabase read;
    private SQLiteDatabase write;
    private ContentValues cv;
    private String tableNameBase;
    private String[] tableBaseColumns;
    private String tableNameBaseInRecipe;
    private String[] tableBaseInRecipeColumns;
    private String tableNameFlavorInRecipe;
    private String[] tableFlavorInRecipeColumns;
    private String tableNameRecipe;
    private String[] tableRecipeColumns;


    private EditText baseName;
    private String baseString;
    private EditText basePg;
    private EditText baseVg;
    private EditText baseWater;
    private EditText baseNicotine;
    private EditText basePrice;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);

        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        if (getIntent().hasExtra(getResources().getStringArray(R.array.bases_table_columns)[0])) {
            deleteBtn.setEnabled(true).setVisible(true);
        } else {
            deleteBtn.setEnabled(false).setVisible(false);
        }

        MenuItem saveBtn = menu.findItem(R.id.menu_save);
        saveBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_save:
                saveBase();
                break;
            case R.id.menu_delete:
                deleteBase();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(CreateBase.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_base);
        this.setTitle(getResources().getString(R.string.create_base_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        read = db.getWritableDatabase();
        write = db.getWritableDatabase();
        cv = new ContentValues();
        tableNameBase = getResources().getString(R.string.bases_table_name);
        tableBaseColumns = getResources().getStringArray(R.array.bases_table_columns);
        tableNameBaseInRecipe = getResources().getString(R.string.base_in_recipes_table_name);
        tableBaseInRecipeColumns = getResources().getStringArray(R.array.base_in_recipes_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        tableFlavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);
        tableNameRecipe = getResources().getString(R.string.recipes_table_name);
        tableRecipeColumns = getResources().getStringArray(R.array.recipes_table_columns);

        baseName = (EditText) findViewById(R.id.newName);
        basePg = (EditText) findViewById(R.id.newPg);
        baseVg = (EditText) findViewById(R.id.newVg);
        baseWater = (EditText) findViewById(R.id.newWasser);
        baseNicotine = (EditText) findViewById(R.id.newNicotine);
        basePrice = (EditText) findViewById(R.id.newPrice);

        if (getIntent().hasExtra(tableBaseColumns[0])) {
            baseName.setText(getIntent().getStringExtra(tableBaseColumns[0]));
            baseString = getIntent().getStringExtra(tableBaseColumns[0]);
            basePg.setText(getIntent().getStringExtra(tableBaseColumns[1]));
            baseVg.setText(getIntent().getStringExtra(tableBaseColumns[2]));
            baseWater.setText(getIntent().getStringExtra(tableBaseColumns[3]));
            baseNicotine.setText(getIntent().getStringExtra(tableBaseColumns[4]));
            basePrice.setText(getIntent().getStringExtra(tableBaseColumns[5]));
        } else {
            baseString = "";
        }
    }

    private void saveBase() {
        String actualName = baseName.getText().toString().trim();
        String actualPg = basePg.getText().toString().trim();
        String actualVg = baseVg.getText().toString().trim();
        String actualWater = baseWater.getText().toString().trim();
        String actualNicotine = baseNicotine.getText().toString().trim();
        String actualPrice = basePrice.getText().toString().trim();
        if (!actualName.equals("") && !actualPg.equals("") && !actualVg.equals("")
                && !actualWater.equals("") && !actualNicotine.equals("") && !actualPrice.equals("")) {
            cv.put(tableBaseColumns[0], actualName);
            cv.put(tableBaseColumns[1], actualPg);
            cv.put(tableBaseColumns[2], actualVg);
            cv.put(tableBaseColumns[3], actualWater);
            cv.put(tableBaseColumns[4], actualNicotine);
            cv.put(tableBaseColumns[5], actualPrice);
            if (getIntent().hasExtra(tableBaseColumns[0])) {
                write.delete(tableNameBase, tableBaseColumns[0] + "=?", new String[]{baseString});
            }
            write.insertWithOnConflict(tableNameBase, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
            Toast.makeText(CreateBase.this, R.string.toast_saved, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(CreateBase.this, R.string.toast_empty_fields, Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteBase() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(CreateBase.this);
        deleteDialog.setMessage(R.string.dialog_clear_base_message);
        deleteDialog.setCancelable(true);

        deleteDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // cursor for base_in_recipe_table
                        final Cursor cursorBaseInRecipe = read.query(tableNameBaseInRecipe, tableBaseInRecipeColumns,
                                tableBaseInRecipeColumns[1] + "=?",
                                new String[]{baseString}, null, null, null, null);

                        if (cursorBaseInRecipe.getCount() > 0) {
                            dialog.cancel();
                            AlertDialog.Builder deleteDialog = new AlertDialog.Builder(CreateBase.this);
                            String message = getResources().getString(R.string.dialog_clear_base_found_in_recipe1) + " " +
                                    cursorBaseInRecipe.getCount() + " " + getResources().getString(R.string.dialog_clear_base_found_in_recipe2) +
                                    " " + getResources().getString(R.string.dialog_clear_base_found_in_recipe3);
                            deleteDialog.setMessage(message);
                            deleteDialog.setCancelable(true);

                            deleteDialog.setPositiveButton(
                                    R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            while (cursorBaseInRecipe.moveToNext()) {
                                                write.delete(tableNameFlavorInRecipe, tableFlavorInRecipeColumns[0] + "=?",
                                                        new String[]{cursorBaseInRecipe.getString(0)});
                                                write.delete(tableNameRecipe, tableRecipeColumns[0] + "=?",
                                                        new String[]{cursorBaseInRecipe.getString(0)});
                                            }
                                            cursorBaseInRecipe.close();
                                            write.delete(tableNameBaseInRecipe, tableBaseInRecipeColumns[1] + "=?",
                                                    new String[]{baseString});
                                            write.delete(tableNameBase, tableBaseColumns[0] + "=?", new String[]{baseString});
                                            dialog.cancel();
                                            Toast.makeText(CreateBase.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                            finish();
                                        }
                                    }
                            );

                            deleteDialog.setNegativeButton(
                                    R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }
                            );

                            AlertDialog alert = deleteDialog.create();
                            alert.show();
                        } else {
                            write.delete(tableNameBase, tableBaseColumns[0] + "=?", new String[]{baseString});
                            dialog.cancel();
                            Toast.makeText(CreateBase.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
        );

        deleteDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDialog.create();
        alert.show();
    }
}
