package de.freezeraag.vapinghelper;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class Mix extends AppCompatActivity {
    // the upper parent layout
    private RelativeLayout relativeLayout;

    // db connection variables
    private SQLiteDatabase read;
    private SQLiteDatabase write;
    private Cursor cursor;

    // Recipe variables
    private HashMap<String, HashMap<String, Float>> recipeValues;
    private String tableNameRecipe;
    private String tableNameBaseInRecipe;
    private String[] baseInRecipeColumns;
    private String tableNameFlavorInRecipe;
    private String[] flavorInRecipeColumns;

    // variables related to all base
    private Button addBase;
    private String tableNameBase;
    private String[] basesColumns;
    private HashMap<String, Float[]> exceptBases;
    private List<String> spinnerArrayBases;
    private Spinner baseSpinner;
    private LinearLayout selectedBases;

    // variables related to all flavor
    private Button addFlavor;
    private String tableNameFlavor;
    private String[] flavorsColumns;
    private HashMap<String, Float[]> exceptFlavors;
    private List<String> spinnerArrayFlavors;
    private Spinner flavorSpinner;
    private LinearLayout selectedFlavors;

    private String[] recipeColumns;
    private String recipeName;
    private float resultPg;
    private float resultVg;
    private float resultWater;
    private float resultNicotine;

    // calculation switch
    private boolean showResult;
    private boolean saveable;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);
        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        if (getIntent().hasExtra("name")) {
            deleteBtn.setEnabled(true).setVisible(true);
        } else {
            deleteBtn.setEnabled(false).setVisible(false);
        }

        MenuItem saveBtn = menu.findItem(R.id.menu_save);
        saveBtn.setEnabled(true).setVisible(true);

        MenuItem calculateBtn = menu.findItem(R.id.menu_calculate);
        calculateBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        InputMethodManager imm;
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_calculate:
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                relativeLayout.requestFocus();
                showResult = !showResult;
                invalidateOptionsMenu();
                move();
                break;
            case R.id.menu_delete:
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                deleteRecipe();
                break;
            case R.id.menu_save:
                if (saveable) {
                    saveRecipe();
                } else {
                    Toast.makeText(Mix.this, R.string.toast_too_much_flavor, Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.menu_settings:
                Intent settings = new Intent(Mix.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem saveBtn = menu.findItem(R.id.menu_save);
        if (showResult) {
            saveBtn.setEnabled(true).setVisible(true);
        } else {
            saveBtn.setEnabled(false).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void fillSpinner(String table, String[] requestedColumns, String orderBy,
                             List<String> arrayList, Spinner spinner, HashMap<String, Float[]> exceptMap) {

        cursor = read.query(table, requestedColumns, null, null, null, null, orderBy + " ASC", null);

        // fill arrayList
        arrayList.clear();
        while (cursor.moveToNext()) {
            String toAdd = "";
            if (table.equals(tableNameBase)) {
                toAdd += cursor.getString(0);
            } else if (table.equals(tableNameFlavor)) {
                toAdd += cursor.getString(0) + " - " + cursor.getString(1);
            }
            if (!exceptMap.containsKey(toAdd)) {
                arrayList.add(toAdd);
            }

            // add arrayList to adapter, then to spinner
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    this, android.R.layout.simple_spinner_item, arrayList);
            spinner.setAdapter(adapter);
        }
        if (arrayList.size() == 0) {
            if (table.equals(tableNameBase)) {
                addBase.setEnabled(false);
                addBase.setAlpha(.5f);
            }
            if (table.equals(tableNameFlavor)) {
                addFlavor.setEnabled(false);
                addFlavor.setAlpha(.5f);
            }
        } else {
            if (table.equals(tableNameBase)) {
                addBase.setEnabled(true);
                addBase.setAlpha(1);
            }
            if (table.equals(tableNameFlavor)) {
                addFlavor.setEnabled(true);
                addFlavor.setAlpha(1);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mix);
        this.setTitle(getResources().getString(R.string.mix_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        relativeLayout = (RelativeLayout) findViewById(R.id.mixLayout);
        showResult = false;
        recipeValues = new HashMap<>();

        // SQLite
        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        read = db.getReadableDatabase();
        write = db.getReadableDatabase();
        tableNameBase = getResources().getString(R.string.bases_table_name);
        basesColumns = getResources().getStringArray(R.array.bases_table_columns);
        exceptBases = new HashMap<>();
        tableNameFlavor = getResources().getString(R.string.flavors_table_name);
        flavorsColumns = getResources().getStringArray(R.array.flavors_table_columns);
        exceptFlavors = new HashMap<>();
        tableNameRecipe = getResources().getString(R.string.recipes_table_name);
        recipeColumns = getResources().getStringArray(R.array.recipes_table_columns);
        tableNameBaseInRecipe = getResources().getString(R.string.base_in_recipes_table_name);
        baseInRecipeColumns = getResources().getStringArray(R.array.base_in_recipes_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        flavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);

        // Spinner - Dropdown
        baseSpinner = (Spinner) findViewById(R.id.baseSpinner);
        spinnerArrayBases = new ArrayList<>();
        flavorSpinner = (Spinner) findViewById(R.id.flavorSpinner);
        spinnerArrayFlavors = new ArrayList<>();

        //BASES
        // LinearLayout for added bases
        selectedBases = (LinearLayout) findViewById(R.id.selectedBases);

        addBase = (Button) findViewById(R.id.add_base_btn);
        if (addBase != null) {
            addBase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spinnerArrayBases.size() > 0) {
                        showResult = false;
                        invalidateOptionsMenu();
                        addBase(baseSpinner.getSelectedItem().toString(), 0);
                        move();
                    }
                }
            });
        }

        fillSpinner(tableNameBase, basesColumns, basesColumns[0], spinnerArrayBases,
                baseSpinner, exceptBases);

        // FLAVORS
        // LinearLayout for added flavors
        selectedFlavors = (LinearLayout) findViewById(R.id.selectedFlavors);

        addFlavor = (Button) findViewById(R.id.add_flavor_btn);
        if (addFlavor != null) {
            addFlavor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spinnerArrayFlavors.size() > 0) {
                        showResult = false;
                        invalidateOptionsMenu();
                        addFlavor(flavorSpinner.getSelectedItem().toString(), 0);
                        move();
                    }
                }
            });
        }

        fillSpinner(tableNameFlavor, flavorsColumns, flavorsColumns[0], spinnerArrayFlavors,
                flavorSpinner, exceptFlavors);

        if (getIntent().hasExtra(recipeColumns[0])) {
            recipeName = getIntent().getStringExtra(recipeColumns[0]);
            // get the bases of the recipe
            Cursor cursorBase = read.query(tableNameBaseInRecipe, baseInRecipeColumns,
                    baseInRecipeColumns[0] + "=?", new String[]{recipeName}, null, null, baseInRecipeColumns[1] + " ASC", null);
            while (cursorBase.moveToNext()) {
                addBase(cursorBase.getString(1), cursorBase.getFloat(2));
            }
            cursorBase.close();

            // get the flavors of the recipe
            Cursor cursorFlavors = read.query(tableNameFlavorInRecipe, flavorInRecipeColumns, flavorInRecipeColumns[0] + "=?", new String[]{recipeName},
                    null, null, flavorInRecipeColumns[1] + " ASC", null);
            while (cursorFlavors.moveToNext()) {
                addFlavor(cursorFlavors.getString(1) + " - " + cursorFlavors.getString(2), cursorFlavors.getFloat(3));
            }
            cursorFlavors.close();
        }
    }

    private void addBase(String baseToAdd, float portion) {
        final LinearLayout linearSelectedBase = (LinearLayout) getLayoutInflater().inflate(R.layout.mix_element_selected, selectedBases, false);
        final TextView selectedHeader = (TextView) linearSelectedBase.findViewById(R.id.selectedHeader);
        selectedHeader.setText(baseToAdd);
        TextView portionHeader = (TextView) linearSelectedBase.findViewById(R.id.portionHeader);
        portionHeader.setText(R.string.mix_base_portion);
        EditText inputField = (EditText) linearSelectedBase.findViewById(R.id.editPortion);
        inputField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v.isFocused()) {
                    showResult = false;
                    invalidateOptionsMenu();
                    move();
                }
            }
        });
        inputField.setId(Math.abs(baseToAdd.hashCode()));
        if (portion != 0) {
            inputField.setText(String.valueOf(portion));
        }

        Button deleteBaseBtn = (Button) linearSelectedBase.findViewById(R.id.btn_delete_selected);
        deleteBaseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResult = false;
                invalidateOptionsMenu();
                move();

                exceptBases.remove(selectedHeader.getText().toString());
                selectedBases.removeView(linearSelectedBase);
                fillSpinner(tableNameBase, basesColumns, basesColumns[0], spinnerArrayBases,
                        baseSpinner, exceptBases);
            }
        });

        // hier query und in hashmap die werte als key eintragen
        cursor = read.query(tableNameBase, basesColumns, basesColumns[0] + "=?", new String[]{baseToAdd},
                null, null, basesColumns[0] + " ASC", null);
        cursor.moveToNext();
        exceptBases.put(baseToAdd, new Float[]{cursor.getFloat(1),
                cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4)});
        fillSpinner(tableNameBase, basesColumns, basesColumns[0], spinnerArrayBases,
                baseSpinner, exceptBases);
        selectedBases.addView(linearSelectedBase);
    }

    private void addFlavor(String flavorToAdd, float portion) {
        final LinearLayout linearSelectedFlavor = (LinearLayout) getLayoutInflater().inflate(R.layout.mix_element_selected, selectedFlavors, false);
        final TextView selectedHeader = (TextView) linearSelectedFlavor.findViewById(R.id.selectedHeader);
        selectedHeader.setText(flavorToAdd);
        TextView portionHeader = (TextView) linearSelectedFlavor.findViewById(R.id.portionHeader);
        portionHeader.setText(R.string.mix_flavor_percent);
        EditText inputField = (EditText) linearSelectedFlavor.findViewById(R.id.editPortion);
        inputField.setId(Math.abs(flavorToAdd.hashCode()));
        inputField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v.isFocused()) {
                    showResult = false;
                    invalidateOptionsMenu();
                    move();
                }
            }
        });
        if (portion != 0) {
            inputField.setText(String.valueOf(portion));
        }

        Button deleteFlavorBtn = (Button) linearSelectedFlavor.findViewById(R.id.btn_delete_selected);
        deleteFlavorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResult = false;
                invalidateOptionsMenu();
                move();

                exceptFlavors.remove(selectedHeader.getText().toString());
                selectedFlavors.removeView(linearSelectedFlavor);
                fillSpinner(tableNameFlavor, flavorsColumns, flavorsColumns[0], spinnerArrayFlavors,
                        flavorSpinner, exceptFlavors);
            }
        });

        exceptFlavors.put(flavorToAdd, new Float[]{});
        fillSpinner(tableNameFlavor, flavorsColumns, flavorsColumns[0], spinnerArrayFlavors,
                flavorSpinner, exceptFlavors);
        selectedFlavors.addView(linearSelectedFlavor);
    }

    private void saveRecipe() {
        if (!recipeValues.containsKey(getString(R.string.bases_table_name))) {
            Toast.makeText(Mix.this, R.string.recipe_without_base, Toast.LENGTH_SHORT).show();
            showResult = false;
            move();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(Mix.this);
            builder.setTitle(R.string.recipe_name_alert);

            // Set up the input
            final EditText input = new EditText(Mix.this);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            if (getIntent().hasExtra(recipeColumns[0])) {
                input.setText(recipeName);
            }

            // Set up the buttons
            builder.setPositiveButton(R.string.dialog_save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String oldRecipeName = "";
                    if (getIntent().hasExtra(recipeColumns[0])) {
                        oldRecipeName = recipeName;
                    }
                    recipeName = input.getText().toString().trim();
                    if (!recipeName.equals("")) {
                        if (getIntent().hasExtra(recipeColumns[0])) {
                            write.delete(tableNameRecipe, recipeColumns[0] + "=?", new String[]{oldRecipeName});
                            write.delete(tableNameBaseInRecipe, baseInRecipeColumns[0] + "=?", new String[]{oldRecipeName});
                            write.delete(tableNameFlavorInRecipe, flavorInRecipeColumns[0] + "=?", new String[]{oldRecipeName});
                        }
                        // db logic for saving a recipe
                        try {
                            ContentValues values = new ContentValues();
                            values.put(recipeColumns[0], recipeName);
                            values.put(recipeColumns[1], resultPg);
                            values.put(recipeColumns[2], resultVg);
                            values.put(recipeColumns[3], resultWater);
                            values.put(recipeColumns[4], resultNicotine);
                            write.insertOrThrow(getString(R.string.recipes_table_name), null, values);
                            for (String base : recipeValues.get(getString(R.string.bases_table_name)).keySet()) {
                                values = new ContentValues();
                                values.put(baseInRecipeColumns[0], recipeName);
                                values.put(baseInRecipeColumns[1], base);
                                values.put(baseInRecipeColumns[2], recipeValues.get(getString(R.string.bases_table_name)).get(base));
                                write.insertOrThrow(getString(R.string.base_in_recipes_table_name), null, values);
                            }
                            if (recipeValues.containsKey(getString(R.string.flavors_table_name))) {
                                for (String flavor : recipeValues.get(getString(R.string.flavors_table_name)).keySet()) {
                                    values = new ContentValues();
                                    values.put(flavorInRecipeColumns[0], recipeName);
                                    values.put(flavorInRecipeColumns[1], flavor.split(" - ")[0]);
                                    values.put(flavorInRecipeColumns[2], flavor.split(" - ")[1]);
                                    values.put(flavorInRecipeColumns[3], recipeValues.get(getString(R.string.flavors_table_name)).get(flavor));
                                    write.insertOrThrow(tableNameFlavorInRecipe, null, values);
                                }
                            }
                            Toast.makeText(Mix.this, R.string.toast_saved, Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (SQLException e) {
                            Toast.makeText(Mix.this, R.string.recipe_name_exists, Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        }
                    } else {
                        Toast.makeText(Mix.this, R.string.toast_name_too_short, Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
    }

    private void deleteRecipe() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Mix.this);
        deleteDialog.setMessage(R.string.dialog_delete_recipe_message);
        deleteDialog.setCancelable(true);

        deleteDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        write.delete(tableNameRecipe, recipeColumns[0] + "=?", new String[]{recipeName});
                        write.delete(tableNameBaseInRecipe, baseInRecipeColumns[0] + "=?", new String[]{recipeName});
                        write.delete(tableNameFlavorInRecipe, flavorInRecipeColumns[0] + "=?", new String[]{recipeName});
                        dialog.cancel();
                        Toast.makeText(Mix.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
        );

        deleteDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDialog.create();
        alert.show();
    }

    private void move() {
        View resultCard = findViewById(R.id.resultView);

        recipeValues.clear();
        if (showResult) {
            ScrollView scroller = (ScrollView) relativeLayout.findViewById(R.id.scrollView);
            scroller.clearFocus();
            // initialize variables for calculation
            resultPg = 0;
            resultVg = 0;
            resultWater = 0;
            resultNicotine = 0;
            float portionsAll = 0;

            // sum all base portions
            for (String base : exceptBases.keySet()) {
                EditText portionThisBase = (EditText) selectedBases.findViewById(Math.abs(base.hashCode()));
                try {
                    if (Float.parseFloat(portionThisBase.getText().toString()) > 0) {
                        portionsAll += Float.parseFloat(portionThisBase.getText().toString());
                        if (!recipeValues.containsKey(tableNameBase)) {
                            recipeValues.put(tableNameBase, new HashMap<String, Float>());
                        }
                        recipeValues.get(tableNameBase).put(base, Float.parseFloat(portionThisBase.getText().toString()));
                    }
                } catch (Exception e) {
                    portionThisBase.setText("0");
                }
            }

            // showResult
            for (String base : exceptBases.keySet()) {
                Float[] values = exceptBases.get(base);
                EditText portionThisBase = (EditText) selectedBases.findViewById(Math.abs(base.hashCode()));

                resultPg += (values[0] / (portionsAll) * Float.parseFloat(portionThisBase.getText().toString()));
                resultVg += (values[1] / (portionsAll) * Float.parseFloat(portionThisBase.getText().toString()));
                resultWater += (values[2] / (portionsAll) * Float.parseFloat(portionThisBase.getText().toString()));
                resultNicotine += (values[3] / (portionsAll) * Float.parseFloat(portionThisBase.getText().toString()));
            }

            // round base values
            resultPg = (float) (((int) (resultPg * 100)) / 100.0);
            resultVg = (float) (((int) (resultVg * 100)) / 100.0);
            resultWater = (float) (((int) (resultWater * 100)) / 100.0);
            resultNicotine = (float) (((int) (resultNicotine * 100)) / 100.0);

            if (resultCard != null) {
                TextView pgView = (TextView) resultCard.findViewById(R.id.resultPg);
                pgView.setText(String.valueOf(resultPg));
                TextView vgView = (TextView) resultCard.findViewById(R.id.resultVg);
                vgView.setText(String.valueOf(resultVg));
                TextView waterView = (TextView) resultCard.findViewById(R.id.resultWater);
                waterView.setText(String.valueOf(resultWater));
                TextView nicotineView = (TextView) resultCard.findViewById(R.id.resultNicotine);
                nicotineView.setText(String.valueOf(resultNicotine));
            }

            // display flavors
            float allFlavorPercentages = 0;
            for (String flavor : exceptFlavors.keySet()) {
                // layout for single elements
                LinearLayout linearLayoutFlavorResults = (LinearLayout) getLayoutInflater().inflate(R.layout.flavor_result_mix_layout, relativeLayout, false);
                TextView singleFlavor = (TextView) linearLayoutFlavorResults.findViewById(R.id.singleFlavorResult);
                EditText portionThisFlavor = (EditText) selectedFlavors.findViewById(Math.abs(flavor.hashCode()));

                try {
                    if (Float.parseFloat(portionThisFlavor.getText().toString()) > 0) {
                        allFlavorPercentages += Float.parseFloat(portionThisFlavor.getText().toString());
                        String flavorText = flavor + ": " + portionThisFlavor.getText().toString() + " " + getString(R.string.percent);
                        singleFlavor.setText(flavorText);
                        if (!recipeValues.containsKey(tableNameFlavor)) {
                            recipeValues.put(tableNameFlavor, new HashMap<String, Float>());
                        }
                        recipeValues.get(tableNameFlavor).put(flavor, Float.parseFloat(portionThisFlavor.getText().toString()));
                    }
                } catch (Exception e) {
                    portionThisFlavor.setText("0");
                }

                // layout where to put the single elements
                LinearLayout flavorResults = (LinearLayout) findViewById(R.id.resultFlavor);
                if (flavorResults != null) {
                    flavorResults.addView(linearLayoutFlavorResults);
                }
            }
            saveable = !(allFlavorPercentages > 50);
        } else {
            // if CardView gets closed destroy all child views
            LinearLayout layout = (LinearLayout) relativeLayout.findViewById(R.id.resultFlavor);
            layout.removeAllViews();
            recipeValues.clear();
        }

        // CardView movement
        TransitionManager.beginDelayedTransition(relativeLayout);
        if (resultCard != null) {
            ViewGroup.LayoutParams size = resultCard.getLayoutParams();

            if (size.height == 0 && showResult) {
                size.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            } else {
                size.height = 0;
            }
            resultCard.setLayoutParams(size);
        }
    }
}
