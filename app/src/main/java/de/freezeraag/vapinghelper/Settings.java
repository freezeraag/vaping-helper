package de.freezeraag.vapinghelper;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class Settings extends AppCompatActivity {

    private static final int PICKFILE_RESULT_CODE = 1;

    // db connection variables
    private SQLiteDatabase read;
    private SQLiteDatabase write;
    private Cursor cursor;

    // Recipe variables
    private String tableNameRecipe;
    private String[] recipeColumns;

    // variables related to all base
    private String tableNameBase;
    private String[] basesColumns;
    private String tableNameBaseInRecipe;
    private String[] baseInRecipeColumns;

    // variables related to all flavor
    private String tableNameFlavor;
    private String[] flavorsColumns;
    private String tableNameFlavorInRecipe;
    private String[] flavorInRecipeColumns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        this.setTitle(getResources().getString(R.string.settings_title));

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        read = db.getWritableDatabase();
        write = db.getWritableDatabase();

        final String tableNameSettings = getResources().getString(R.string.settings_table_name);
        final String[] tableSettingsColumns = getResources().getStringArray(R.array.settings_table_columns);
        final String[] tableSettingsKeys = getResources().getStringArray(R.array.settings_keys);
        tableNameRecipe = getResources().getString(R.string.recipes_table_name);
        recipeColumns = getResources().getStringArray(R.array.recipes_table_columns);
        tableNameBase = getResources().getString(R.string.bases_table_name);
        basesColumns = getResources().getStringArray(R.array.bases_table_columns);
        tableNameBaseInRecipe = getResources().getString(R.string.base_in_recipes_table_name);
        baseInRecipeColumns = getResources().getStringArray(R.array.base_in_recipes_table_columns);
        tableNameFlavor = getResources().getString(R.string.flavors_table_name);
        flavorsColumns = getResources().getStringArray(R.array.flavors_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        flavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);

        CheckBox notify = (CheckBox) findViewById(R.id.allow_notifications);
        Cursor cursor = read.query(tableNameSettings, tableSettingsColumns, tableSettingsColumns[0] + "=?",
                new String[]{tableSettingsKeys[0]}, null, null, null);
        cursor.moveToNext();
        if (notify != null) {
            if (cursor.getString(1).equals("true")) {
                notify.setChecked(true);
            } else {
                notify.setChecked(false);
            }
            cursor.close();

            notify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ContentValues cv = new ContentValues();
                    if (isChecked) {
                        cv.put(tableSettingsColumns[0], tableSettingsKeys[0]);
                        cv.put(tableSettingsColumns[1], "true");
                    } else {
                        cv.put(tableSettingsColumns[0], tableSettingsKeys[0]);
                        cv.put(tableSettingsColumns[1], "false");
                    }
                    write.insertWithOnConflict(tableNameSettings, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                }
            });
        }

        CheckBox vibrate = (CheckBox) findViewById(R.id.allow_notifications_vibrate);
        cursor = read.query(tableNameSettings, tableSettingsColumns, tableSettingsColumns[0] + "=?",
                new String[]{tableSettingsKeys[1]}, null, null, null);
        cursor.moveToNext();
        if (vibrate != null) {
            if (cursor.getString(1).equals("true")) {
                vibrate.setChecked(true);
            } else {
                vibrate.setChecked(false);
            }
            cursor.close();

            vibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ContentValues cv = new ContentValues();
                    if (isChecked) {
                        cv.put(tableSettingsColumns[0], tableSettingsKeys[1]);
                        cv.put(tableSettingsColumns[1], "true");
                    } else {
                        cv.put(tableSettingsColumns[0], tableSettingsKeys[1]);
                        cv.put(tableSettingsColumns[1], "false");
                    }
                    write.insertWithOnConflict(tableNameSettings, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                }
            });
        }

        Button exportBtn = (Button) findViewById(R.id.export_btn);
        if (exportBtn != null) {
            exportBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exportDB();
                }
            });
        }


        Button importBtn = (Button) findViewById(R.id.import_btn);
        if (importBtn != null) {
            importBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    importDB();
                }
            });
        }
    }

    private void importDB() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        startActivityForResult(intent, PICKFILE_RESULT_CODE);
    }

    private void exportDB() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);
        builder.setTitle(R.string.file_name_alert);

        // Set up the input
        final EditText input = new EditText(Settings.this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(R.string.dialog_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fileName = input.getText().toString().trim();
                if (!fileName.equals("")) {
                    try {
                        File root = new File(Environment.getExternalStorageDirectory(), "VapingHelper");
                        // if external memory exists and folder with name Notes
                        if (!root.exists()) {
                            root.mkdirs(); // this will create folder.
                        }
                        File filepath = new File(root, fileName + ".txt");  // file path to save
                        FileWriter fileWriter = new FileWriter(filepath);
                        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);


                        JSONObject jsonDatabase = new JSONObject();
                        // store bases
                        cursor = read.query(tableNameBase, basesColumns, null, null, null, null,
                                basesColumns[0] + " ASC", null);
                        JSONArray jsonAllBases = new JSONArray();
                        while (cursor.moveToNext()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(basesColumns[0], cursor.getString(0));
                            jsonObject.put(basesColumns[1], cursor.getFloat(1));
                            jsonObject.put(basesColumns[2], cursor.getFloat(2));
                            jsonObject.put(basesColumns[3], cursor.getFloat(3));
                            jsonObject.put(basesColumns[4], cursor.getFloat(4));
                            jsonObject.put(basesColumns[5], cursor.getFloat(5));
                            jsonAllBases.put(jsonObject);
                        }
                        jsonDatabase.put(tableNameBase, jsonAllBases);
                        cursor.close();

                        // store flavors
                        cursor = read.query(tableNameFlavor, flavorsColumns, null, null, null, null,
                                flavorsColumns[0] + " ASC", null);
                        JSONArray jsonAllFlavors = new JSONArray();
                        while (cursor.moveToNext()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(flavorsColumns[0], cursor.getString(0));
                            jsonObject.put(flavorsColumns[1], cursor.getString(1));
                            jsonObject.put(flavorsColumns[2], cursor.getFloat(2));
                            jsonAllFlavors.put(jsonObject);
                        }
                        jsonDatabase.put(tableNameFlavor, jsonAllFlavors);
                        cursor.close();

                        // store recipes
                        cursor = read.query(tableNameRecipe, recipeColumns,
                                null, null, null, null, recipeColumns[0] + " ASC", null);
                        JSONArray jsonAllRecipes = new JSONArray();
                        while (cursor.moveToNext()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(recipeColumns[0], cursor.getString(0));
                            jsonObject.put(recipeColumns[1], cursor.getFloat(1));
                            jsonObject.put(recipeColumns[2], cursor.getFloat(2));
                            jsonObject.put(recipeColumns[3], cursor.getFloat(3));
                            jsonObject.put(recipeColumns[4], cursor.getFloat(4));
                            jsonObject.put(recipeColumns[5], cursor.getFloat(5));
                            jsonObject.put(recipeColumns[6], cursor.getString(6));
                            jsonAllRecipes.put(jsonObject);
                        }
                        jsonDatabase.put(tableNameRecipe, jsonAllRecipes);
                        cursor.close();

                        // store bases of recipes
                        cursor = read.query(tableNameBaseInRecipe, baseInRecipeColumns,
                                null, null, null, null, baseInRecipeColumns[0] + " ASC", null);
                        JSONArray jsonAllBasesRecipes = new JSONArray();
                        while (cursor.moveToNext()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(baseInRecipeColumns[0], cursor.getString(0));
                            jsonObject.put(baseInRecipeColumns[1], cursor.getString(1));
                            jsonObject.put(baseInRecipeColumns[2], cursor.getFloat(2));
                            jsonAllBasesRecipes.put(jsonObject);
                        }
                        jsonDatabase.put(tableNameBaseInRecipe, jsonAllBasesRecipes);
                        cursor.close();

                        // store flavors of recipes
                        cursor = read.query(tableNameFlavorInRecipe, flavorInRecipeColumns,
                                null, null, null, null, flavorInRecipeColumns[0] + " ASC", null);
                        JSONArray jsonAllFlavorsRecipes = new JSONArray();
                        while (cursor.moveToNext()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(flavorInRecipeColumns[0], cursor.getString(0));
                            jsonObject.put(flavorInRecipeColumns[1], cursor.getString(1));
                            jsonObject.put(flavorInRecipeColumns[2], cursor.getString(2));
                            jsonObject.put(flavorInRecipeColumns[3], cursor.getFloat(3));
                            jsonAllFlavorsRecipes.put(jsonObject);
                        }
                        jsonDatabase.put(tableNameFlavorInRecipe, jsonAllFlavorsRecipes);
                        cursor.close();

                        // add json-Object to file
                        bufferedWriter.write(jsonDatabase.toString());
                        bufferedWriter.flush();
                        bufferedWriter.close();
                        fileWriter.close();
                        cursor.close();
                        Toast.makeText(Settings.this, R.string.toast_saved, Toast.LENGTH_SHORT).show();
                        finish();

                    } catch (Exception e) {
                        Toast.makeText(Settings.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(Settings.this, getString(R.string.toast_file_create_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Settings.this, R.string.toast_name_too_short, Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }
        );
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String jsonString;
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                try {
                    Uri uri = data.getData();
                    jsonString = readTextFromUri(uri);
                    writeToDB(jsonString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "Error reading text from file.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void writeToDB(String jsonString) {
        try {
            JSONObject jsonDatabase = new JSONObject(jsonString);
            if (jsonDatabase.has(tableNameBase)) {
                // import bases
                JSONArray basesArray = jsonDatabase.getJSONArray(tableNameBase);
                for (int index = 0; index < basesArray.length(); index++) {
                    ContentValues cv = new ContentValues();
                    JSONObject base = (JSONObject) basesArray.get(index);
                    String name = base.getString(basesColumns[0]).trim();
                    String pg = base.getString(basesColumns[1]).trim();
                    String vg = base.getString(basesColumns[2]).trim();
                    String water = base.getString(basesColumns[3]).trim();
                    String nicotine = base.getString(basesColumns[4]).trim();
                    String price = "0.0";
                    if (base.has(basesColumns[5])) {
                        price = base.getString(basesColumns[5]).trim();
                    }
                    if (!name.equals("") && !pg.equals("") && !vg.equals("")
                            && !water.equals("") && !nicotine.equals("") && !price.equals("")) {
                        cv.put(basesColumns[0], name);
                        cv.put(basesColumns[1], pg);
                        cv.put(basesColumns[2], vg);
                        cv.put(basesColumns[3], water);
                        cv.put(basesColumns[4], nicotine);
                        cv.put(basesColumns[5], price);
                        write.insertWithOnConflict(tableNameBase, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                    }
                }
            }
            if (jsonDatabase.has(tableNameFlavor)) {
                // import flavors
                JSONArray flavorsArray = jsonDatabase.getJSONArray(tableNameFlavor);
                for (int index = 0; index < flavorsArray.length(); index++) {
                    ContentValues cv = new ContentValues();
                    JSONObject flavor = (JSONObject) flavorsArray.get(index);
                    String manufacturer = flavor.getString(flavorsColumns[0]).trim();
                    String flavorType = flavor.getString(flavorsColumns[1]).trim();
                    String flavorPrice = "0.0";
                    if (flavor.has(flavorsColumns[2])) {
                        flavor.getString(flavorsColumns[2]).trim();
                    }
                    if (!manufacturer.equals("") && !flavorType.equals("") && !flavorPrice.equals("")) {
                        cv.put(flavorsColumns[0], manufacturer);
                        cv.put(flavorsColumns[1], flavorType);
                        cv.put(flavorsColumns[2], flavorPrice);
                        write.insertWithOnConflict(tableNameFlavor, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                    }
                }
            }
            if (jsonDatabase.has(tableNameRecipe)) {
                // import recipes
                JSONArray recipesArray = jsonDatabase.getJSONArray(tableNameRecipe);
                for (int index = 0; index < recipesArray.length(); index++) {
                    boolean writable = false;
                    ContentValues cvRecipe = new ContentValues();
                    JSONObject recipe = (JSONObject) recipesArray.get(index);
                    String name = recipe.getString(recipeColumns[0]).trim();
                    String pg = recipe.getString(recipeColumns[1]).trim();
                    String vg = recipe.getString(recipeColumns[2]).trim();
                    String water = recipe.getString(recipeColumns[3]).trim();
                    String nicotine = recipe.getString(recipeColumns[4]).trim();
                    String steeping = recipe.getString(recipeColumns[5]).trim();
                    String steeping_measure = recipe.getString(recipeColumns[6]).trim();
                    if (!name.equals("") && !pg.equals("") && !pg.equals("") && !vg.equals("") && !nicotine.equals("") && !steeping.equals("") && !steeping_measure.equals("")) {
                        cvRecipe.put(recipeColumns[0], name);
                        cvRecipe.put(recipeColumns[1], pg);
                        cvRecipe.put(recipeColumns[2], vg);
                        cvRecipe.put(recipeColumns[3], water);
                        cvRecipe.put(recipeColumns[4], nicotine);
                        cvRecipe.put(recipeColumns[5], steeping);
                        cvRecipe.put(recipeColumns[6], steeping_measure);
                    }

                    if (jsonDatabase.has(tableNameBaseInRecipe)) {
                        // import bases of recipes
                        JSONArray baseInRecipeArray = jsonDatabase.getJSONArray(tableNameBaseInRecipe);
                        for (int indexBIR = 0; indexBIR < baseInRecipeArray.length(); indexBIR++) {
                            ContentValues cvBIR = new ContentValues();
                            JSONObject baseInRecipe = (JSONObject) baseInRecipeArray.get(indexBIR);
                            String recipe_fk = baseInRecipe.getString(baseInRecipeColumns[0]).trim();
                            String name_bases_fk = baseInRecipe.getString(baseInRecipeColumns[1]).trim();
                            String portion = baseInRecipe.getString(baseInRecipeColumns[2]).trim();
                            if (recipe_fk.equals(name) && !name_bases_fk.equals("") && !portion.equals("")) {
                                cvBIR.put(baseInRecipeColumns[0], recipe_fk);
                                cvBIR.put(baseInRecipeColumns[1], name_bases_fk);
                                cvBIR.put(baseInRecipeColumns[2], portion);
                                write.insertWithOnConflict(tableNameBaseInRecipe, null, cvBIR, SQLiteDatabase.CONFLICT_REPLACE);
                                writable |= true;
                            }
                        }
                    }
                    if (jsonDatabase.has(tableNameFlavorInRecipe)) {
                        // import flavors of recipes
                        JSONArray flavorInRecipeArray = jsonDatabase.getJSONArray(tableNameFlavorInRecipe);
                        for (int indexFIR = 0; indexFIR < flavorInRecipeArray.length(); indexFIR++) {
                            ContentValues cvFIR = new ContentValues();
                            JSONObject flavorInRecipe = (JSONObject) flavorInRecipeArray.get(indexFIR);
                            String recipe_fk = flavorInRecipe.getString(flavorInRecipeColumns[0]).trim();
                            String manufacturer_flavors_fk = flavorInRecipe.getString(flavorInRecipeColumns[1]).trim();
                            String flavor_flavors_fk = flavorInRecipe.getString(flavorInRecipeColumns[2]).trim();
                            String portion = flavorInRecipe.getString(flavorInRecipeColumns[3]).trim();
                            if (recipe_fk.equals(name) && !manufacturer_flavors_fk.equals("") && !flavor_flavors_fk.equals("") && !portion.equals("")) {
                                cvFIR.put(flavorInRecipeColumns[0], recipe_fk);
                                cvFIR.put(flavorInRecipeColumns[1], manufacturer_flavors_fk);
                                cvFIR.put(flavorInRecipeColumns[2], flavor_flavors_fk);
                                cvFIR.put(flavorInRecipeColumns[3], portion);
                                write.insertWithOnConflict(tableNameFlavorInRecipe, null, cvFIR, SQLiteDatabase.CONFLICT_REPLACE);
                            }
                        }
                    }
                    if (writable) {
                        write.insertWithOnConflict(tableNameRecipe, null, cvRecipe, SQLiteDatabase.CONFLICT_REPLACE);
                    }
                }
            }
            Toast.makeText(Settings.this, getResources().getString(R.string.data_added), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Toast.makeText(Settings.this, getResources().getString(R.string.no_valid_json_alert), Toast.LENGTH_SHORT).show();
        }
    }

    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = null;
        if (inputStream != null) {
            reader = new BufferedReader(new InputStreamReader(inputStream));
        }
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        if (reader != null) {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
