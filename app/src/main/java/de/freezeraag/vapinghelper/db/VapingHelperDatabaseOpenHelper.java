package de.freezeraag.vapinghelper.db;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import de.freezeraag.vapinghelper.R;

/**
 * Created by Michael Lorenz
 * 03.05.2016.
 *
 * added price columns for bases and flavors
 * 13.06.2016.
 */

public class VapingHelperDatabaseOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "LiquidKochbuch_db";
    private static final int DATABASE_VERSION = 3;

    private Resources r;

    public VapingHelperDatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // Application resources
        this.r = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + r.getString(R.string.bases_table_name) + "(" +
                r.getStringArray(R.array.bases_table_columns)[0] + " TEXT PRIMARY KEY," +
                r.getStringArray(R.array.bases_table_columns)[1] + " REAL," +
                r.getStringArray(R.array.bases_table_columns)[2] + " REAL," +
                r.getStringArray(R.array.bases_table_columns)[3] + " REAL," +
                r.getStringArray(R.array.bases_table_columns)[4] + " REAL);");

        db.execSQL("CREATE TABLE " + r.getString(R.string.flavors_table_name) + "(" +
                r.getStringArray(R.array.flavors_table_columns)[0] + " TEXT," +
                r.getStringArray(R.array.flavors_table_columns)[1] + " TEXT," +
                "PRIMARY KEY (" + r.getStringArray(R.array.flavors_table_columns)[0] + ", " + r.getStringArray(R.array.flavors_table_columns)[1] + "));");

        db.execSQL("CREATE TABLE " + r.getString(R.string.recipes_table_name) + "(" +
                r.getStringArray(R.array.recipes_table_columns)[0] + " TEXT PRIMARY KEY," +
                r.getStringArray(R.array.recipes_table_columns)[1] + " REAL," +
                r.getStringArray(R.array.recipes_table_columns)[2] + " REAL," +
                r.getStringArray(R.array.recipes_table_columns)[3] + " REAL," +
                r.getStringArray(R.array.recipes_table_columns)[4] + " REAL," +
                r.getStringArray(R.array.recipes_table_columns)[5] + " REAL," +
                r.getStringArray(R.array.recipes_table_columns)[6] + " TEXT);");

        db.execSQL("CREATE TABLE " + r.getString(R.string.base_in_recipes_table_name) + "(" +
                r.getStringArray(R.array.base_in_recipes_table_columns)[0] + " TEXT," +
                r.getStringArray(R.array.base_in_recipes_table_columns)[1] + " TEXT," +
                r.getStringArray(R.array.base_in_recipes_table_columns)[2] + " REAL," +
                "PRIMARY KEY (" + r.getStringArray(R.array.base_in_recipes_table_columns)[0] + ", " + r.getStringArray(R.array.base_in_recipes_table_columns)[1] + "));");

        db.execSQL("CREATE TABLE " + r.getString(R.string.flavor_in_recipes_table_name) + "(" +
                r.getStringArray(R.array.flavor_in_recipes_table_columns)[0] + " TEXT," +
                r.getStringArray(R.array.flavor_in_recipes_table_columns)[1] + " TEXT," +
                r.getStringArray(R.array.flavor_in_recipes_table_columns)[2] + " TEXT," +
                r.getStringArray(R.array.flavor_in_recipes_table_columns)[3] + " REAL," +
                "PRIMARY KEY (" + r.getStringArray(R.array.flavor_in_recipes_table_columns)[0] + ", " +
                r.getStringArray(R.array.flavor_in_recipes_table_columns)[1] + ", " +
                r.getStringArray(R.array.flavor_in_recipes_table_columns)[2] + "));");

        db.execSQL("CREATE TABLE " + r.getString(R.string.steepings_table_name) + "(" +
                r.getStringArray(R.array.steepings_table_columns)[0] + " TEXT," +
                r.getStringArray(R.array.steepings_table_columns)[1] + " INTEGER," +
                r.getStringArray(R.array.steepings_table_columns)[2] + " INTEGER," +
                r.getStringArray(R.array.steepings_table_columns)[3] + " TEXT," +
                r.getStringArray(R.array.steepings_table_columns)[4] + " REAL," +
                "PRIMARY KEY (" + r.getStringArray(R.array.steepings_table_columns)[0] + ", " +
                r.getStringArray(R.array.steepings_table_columns)[1] + "));");

        db.execSQL("CREATE TABLE " + r.getString(R.string.settings_table_name) + "(" +
                r.getStringArray(R.array.settings_table_columns)[0] + " TEXT PRIMARY KEY," +
                r.getStringArray(R.array.settings_table_columns)[1] + " TEXT);");
        db.execSQL("INSERT INTO settings (setting, set_value)" +
                " VALUES ('allow_notification', 'true');");
        db.execSQL("INSERT INTO settings (setting, set_value)" +
                " VALUES ('allow_notification_vibrate', 'true');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 3) {
            db.execSQL("ALTER TABLE "+ r.getString(R.string.bases_table_name) + " ADD COLUMN " +
                    r.getStringArray(R.array.bases_table_columns)[5] + " REAL;");
            db.execSQL("ALTER TABLE "+ r.getString(R.string.flavors_table_name) + " ADD COLUMN " +
                    r.getStringArray(R.array.flavors_table_columns)[2] + " REAL;");
        }
    }
}
