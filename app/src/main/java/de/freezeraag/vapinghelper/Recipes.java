package de.freezeraag.vapinghelper;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Arrays;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class Recipes extends AppCompatActivity {

    private SQLiteDatabase read;
    private SQLiteDatabase write;
    private String tableNameRecipes;
    private String[] tableRecipesColumns;
    private String tableNameBaseInRecipe;
    private String[] tableBaseInRecipeColumns;
    private String tableNameFlavorInRecipe;
    private String[] tableFlavorInRecipeColumns;
    private String tableNameSteepings;
    private String[] tableSteepingsColumns;
    private Cursor cursor;
    private RelativeLayout relativeLayout;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);

        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        deleteBtn.setEnabled(true).setVisible(true);

        MenuItem addBtn = menu.findItem(R.id.menu_add);
        addBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_add:
                newRecipe();
                break;
            case R.id.menu_delete:
                clearEntries();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(Recipes.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        this.setTitle(getResources().getString(R.string.recipes_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        read = db.getReadableDatabase();
        write = db.getWritableDatabase();
        tableNameRecipes = getResources().getString(R.string.recipes_table_name);
        tableRecipesColumns = getResources().getStringArray(R.array.recipes_table_columns);
        tableNameBaseInRecipe = getResources().getString(R.string.base_in_recipes_table_name);
        tableBaseInRecipeColumns = getResources().getStringArray(R.array.base_in_recipes_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        tableFlavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);
        tableNameSteepings = getResources().getString(R.string.steepings_table_name);
        tableSteepingsColumns = getResources().getStringArray(R.array.steepings_table_columns);

        cursor = read.query(tableNameRecipes, tableRecipesColumns,
                null, null, null, null, tableRecipesColumns[0] + " ASC", null);
        relativeLayout = (RelativeLayout) findViewById(R.id.myRecipes);
        int cardNumber = 0;

        while (cursor.moveToNext()) {
            final String name = cursor.getString(0);

            CardView card = (CardView) getLayoutInflater().inflate(R.layout.recipe_element_layout, relativeLayout, false);
            final RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams) card.getLayoutParams();

            final int cardId = R.string.card_id + cardNumber;
            card.setId(cardId);
            if (cardNumber > 0) {
                relativeParams.addRule(RelativeLayout.BELOW, cardId - 1);
            }
            cardNumber++;
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    move(cardId);
                }
            };

            View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    TextView header = (TextView) v.findViewById(R.id.recipeTextViewHeader);
                    final String recipeName = header.getText().toString();

                    AlertDialog.Builder deleteDbDialog = new AlertDialog.Builder(Recipes.this);
                    deleteDbDialog.setMessage(R.string.dialog_recipe_longClick_message);
                    deleteDbDialog.setCancelable(true);

                    deleteDbDialog.setPositiveButton(
                            R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Recipes.this, Mix.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString(tableRecipesColumns[0], recipeName);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                    finish();
                                    dialog.cancel();
                                }
                            }
                    );

                    deleteDbDialog.setNegativeButton(
                            R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );

                    AlertDialog alert = deleteDbDialog.create();
                    alert.show();
                    return true;
                }
            };

            card.setOnClickListener(listener);
            card.setOnLongClickListener(longClickListener);

            Button aRecipe = (Button) card.findViewById(R.id.recipeBtn);
            String btnText = name.charAt(0) + "";
            aRecipe.setText(btnText);
            aRecipe.setOnClickListener(listener);

            ImageButton steepRecipeBtn = (ImageButton) card.findViewById(R.id.recipeSteepBtn);
            steepRecipeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder startSteepDialog = new AlertDialog.Builder(Recipes.this);
                    startSteepDialog.setMessage(R.string.dialog_steep_request);
                    startSteepDialog.setCancelable(true);

                    startSteepDialog.setPositiveButton(
                            R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    addSteeping(cardId);
                                    dialog.cancel();
                                }
                            }
                    );

                    startSteepDialog.setNegativeButton(
                            R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );

                    AlertDialog alert = startSteepDialog.create();
                    alert.show();
                }
            });

            TextView textViewHeader = (TextView) card.findViewById(R.id.recipeTextViewHeader);
            textViewHeader.setText(name);

            TextView textViewSubHeader = (TextView) card.findViewById(R.id.recipeTextViewSubHeader);
            String portions = cursor.getString(1) + "/" + cursor.getString(2) + "/" + cursor.getString(3) + "   " +
                    getString(R.string.nicotine_short) + ": " + cursor.getString(4) + " " + getString(R.string.milligrammPerMilliliter);

            textViewSubHeader.setText(portions);

            card.setLayoutParams(relativeParams);
            relativeLayout.addView(card);
        }
    }

    private void addSteeping(int id) {
        final View card = findViewById(id);
        if (card != null) {
            final String recipeName = ((TextView) card.findViewById(R.id.recipeTextViewHeader)).getText().toString();
            final float amount = Float.parseFloat(((EditText) card.findViewById(R.id.amountOfLiquid)).getText().toString());
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.steeping_time_alert);

            LinearLayout dialogLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.add_steeping_dialog_layout, relativeLayout, false);

            // Set up the input
            final EditText input = (EditText) dialogLayout.findViewById(R.id.steeping_time_input);
            input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

            final Spinner spinner = (Spinner) dialogLayout.findViewById(R.id.steeping_measures_spinner);
            ArrayList<String> spinnerArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.steeping_measures)));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    this, android.R.layout.simple_spinner_item, spinnerArray);
            spinner.setAdapter(adapter);

            builder.setView(dialogLayout);

            // Set up the buttons
            builder.setPositiveButton(R.string.dialog_save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ContentValues cv = new ContentValues();
                    long millis = System.currentTimeMillis();
                    cv.put(tableSteepingsColumns[0], recipeName);
                    cv.put(tableSteepingsColumns[1], millis);
                    cv.put(tableSteepingsColumns[2], input.getText().toString());
                    cv.put(tableSteepingsColumns[3], spinner.getSelectedItem().toString());
                    cv.put(tableSteepingsColumns[4], amount);
                    write.insertWithOnConflict(tableNameSteepings, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                    addNotification(recipeName, millis, Float.parseFloat(input.getText().toString()), spinner.getSelectedItem().toString());
                    Intent reload = getIntent();
                    startActivity(reload);
                    dialog.cancel();
                    finish();
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
    }

    // whenever a steeping is set to start a timer will be set on which end a notification will appear
    private void addNotification(String recipe, long startTime, float duration, String measure) {
        NotificationPublisher.getInstance(this).addNotification(this, recipe, startTime, duration, measure);
    }

    private void move(int id) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
        final View card = findViewById(id);
        if (card != null) {
            final String recipeName = ((TextView) card.findViewById(R.id.recipeTextViewHeader)).getText().toString();
            // layout where ingredient are put in
            final LinearLayout contentLayout = (LinearLayout) card.findViewById(R.id.recipe_element_content);
            final LinearLayout ingredientsContainer = (LinearLayout) card.findViewById(R.id.recipe_ingredients_container);

            Button calculate = (Button) card.findViewById(R.id.btn_calculate_recipe);
            calculate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                    ingredientsContainer.removeAllViews();

                    EditText amountInput = (EditText) card.findViewById(R.id.amountOfLiquid);

                    // initialize variables for calculation
                    if (!amountInput.getText().toString().equals("")) {

                        // let the buttons swap
                        TransitionManager.beginDelayedTransition(relativeLayout);
                        ImageButton steepBtn = (ImageButton) card.findViewById(R.id.recipeSteepBtn);
                        steepBtn.setVisibility(View.VISIBLE);
                        Button recipeBtn = (Button) card.findViewById(R.id.recipeBtn);
                        recipeBtn.setVisibility(View.GONE);

                        float wholeAmount = Float.parseFloat(amountInput.getText().toString());
                        float portionsAllBases = 0;
                        float portionsAllFlavors = 0;

                        // sum all flavor portions
                        cursor = read.query(tableNameFlavorInRecipe, tableFlavorInRecipeColumns,
                                tableFlavorInRecipeColumns[0] + "=?", new String[]{recipeName},
                                null, null, tableFlavorInRecipeColumns[1] + " ASC", null);
                        while (cursor.moveToNext()) {
                            portionsAllFlavors += cursor.getFloat(3);
                        }

                        // sum all base portions
                        cursor = read.query(tableNameBaseInRecipe, tableBaseInRecipeColumns,
                                tableBaseInRecipeColumns[0] + "=?", new String[]{recipeName},
                                null, null, tableBaseInRecipeColumns[1] + " ASC", null);
                        while (cursor.moveToNext()) {
                            portionsAllBases += cursor.getFloat(2);
                        }

                        // put in base values
                        cursor = read.query(tableNameBaseInRecipe, tableBaseInRecipeColumns,
                                tableBaseInRecipeColumns[0] + "=?", new String[]{recipeName},
                                null, null, tableBaseInRecipeColumns[1] + " ASC", null);
                        while (cursor.moveToNext()) {
                            LinearLayout ingredientView = (LinearLayout) getLayoutInflater().inflate(R.layout.recipe_element_calculated_content_layout, contentLayout, false);
                            TextView ingredient = (TextView) ingredientView.findViewById(R.id.recipe_ingredient);
                            ingredient.setText(cursor.getString(1));
                            TextView ingredientAmount = (TextView) ingredientView.findViewById(R.id.recipe_ingredient_amount);
                            float amount = ((wholeAmount * ((100 - portionsAllFlavors) / 100)) / portionsAllBases) * cursor.getFloat(2);
                            amount = (float) (((int) (amount * 100)) / 100.0);
                            String amountText = amount + " " + getString(R.string.milliliter);
                            ingredientAmount.setText(amountText);
                            ingredientsContainer.addView(ingredientView);
                        }

                        // put in flavor values
                        cursor = read.query(tableNameFlavorInRecipe, tableFlavorInRecipeColumns,
                                tableFlavorInRecipeColumns[0] + "=?", new String[]{recipeName},
                                null, null, tableFlavorInRecipeColumns[1] + " ASC", null);
                        while (cursor.moveToNext()) {
                            LinearLayout ingredientView = (LinearLayout) getLayoutInflater().inflate(R.layout.recipe_element_calculated_content_layout, contentLayout, false);
                            TextView ingredient = (TextView) ingredientView.findViewById(R.id.recipe_ingredient);
                            String flavorString = cursor.getString(1) + " - " + cursor.getString(2);
                            ingredient.setText(flavorString);
                            TextView ingredientAmount = (TextView) ingredientView.findViewById(R.id.recipe_ingredient_amount);
                            float amount = wholeAmount * (cursor.getFloat(3) / 100);
                            amount = (float) ((Math.round(amount * 100)) / 100.0);
                            String amountText = " " + amount + " " + getString(R.string.milliliter);
                            ingredientAmount.setText(amountText);
                            ingredientsContainer.addView(ingredientView);
                        }
                    }
                }
            });

            // fade in calculation option
            TransitionManager.beginDelayedTransition(relativeLayout);

            ViewGroup.LayoutParams size = card.getLayoutParams();
            if (contentLayout.getVisibility() == View.GONE) {
                contentLayout.setVisibility(View.VISIBLE);
            } else {
                contentLayout.setVisibility(View.GONE);
                Button recipeBtn = (Button) card.findViewById(R.id.recipeBtn);
                if (recipeBtn.getVisibility() == View.GONE) {
                    TransitionManager.beginDelayedTransition(relativeLayout);
                    ImageButton steepBtn = (ImageButton) card.findViewById(R.id.recipeSteepBtn);
                    steepBtn.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(relativeLayout);
                    recipeBtn.setVisibility(View.VISIBLE);
                }

                ingredientsContainer.removeAllViews();
            }
            size.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            card.setLayoutParams(size);
        }
    }

    private void newRecipe() {
        Intent openNewRecipe = new Intent(this, Mix.class);
        startActivity(openNewRecipe);
        finish();
    }

    private void clearEntries() {
        AlertDialog.Builder deleteDbDialog = new AlertDialog.Builder(Recipes.this);
        deleteDbDialog.setMessage(R.string.dialog_clear_table_recipes_message);
        deleteDbDialog.setCancelable(true);

        deleteDbDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        write.delete(tableNameRecipes, null, null);
                        write.delete(tableNameBaseInRecipe, null, null);
                        write.delete(tableNameFlavorInRecipe, null, null);
                        Intent reload = getIntent();
                        startActivity(reload);
                        finish();
                        dialog.cancel();
                    }
                }
        );

        deleteDbDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDbDialog.create();
        alert.show();
    }
}