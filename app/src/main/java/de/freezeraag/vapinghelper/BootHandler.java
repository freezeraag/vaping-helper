package de.freezeraag.vapinghelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

/**
 * Created by Michael Lorenz
 * 05.05.2016.
 */
public class BootHandler extends BroadcastReceiver {

    String tableNameSteepings;
    String[] tableSteepingsColumns;

    public void onReceive(Context context, Intent intent) {
        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(context);
        SQLiteDatabase read = db.getReadableDatabase();
        tableNameSteepings = context.getResources().getString(R.string.steepings_table_name);
        tableSteepingsColumns = context.getResources().getStringArray(R.array.steepings_table_columns);

        Cursor cursor = read.query(tableNameSteepings, tableSteepingsColumns,
                null, null, null, null, tableSteepingsColumns[0] + " ASC", null);

        while (cursor.moveToNext()) {
            String recipe = cursor.getFloat(4) + " " + context.getResources().getString(R.string.milliliter) +
                    " '" + cursor.getString(0) + "' " + context.getResources().getString(R.string.notification_isReady);
            NotificationPublisher.getInstance(context).addNotification(context, recipe, cursor.getLong(1), cursor.getFloat(2), cursor.getString(3));
        }
        cursor.close();
    }
}
