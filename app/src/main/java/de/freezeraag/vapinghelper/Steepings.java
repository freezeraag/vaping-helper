package de.freezeraag.vapinghelper;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class Steepings extends AppCompatActivity {

    private SQLiteDatabase write;
    private String tableNameSteepings;
    private String[] tableSteepingsColumns;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);

        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        deleteBtn.setEnabled(true).setVisible(true);

        MenuItem addBtn = menu.findItem(R.id.menu_add);
        addBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_add:
                Intent recipes = new Intent(Steepings.this, Recipes.class);
                startActivity(recipes);
                break;
            case R.id.menu_delete:
                clearEntries();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(Steepings.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steepings);
        this.setTitle(getResources().getString(R.string.steepings_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.mySteepings);

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        SQLiteDatabase read = db.getReadableDatabase();
        write = db.getReadableDatabase();
        tableNameSteepings = getResources().getString(R.string.steepings_table_name);
        tableSteepingsColumns = getResources().getStringArray(R.array.steepings_table_columns);

        final Cursor cursor = read.query(tableNameSteepings, tableSteepingsColumns, null, null, null, null, null, null);
        int cardNumber = 0;
        while (cursor.moveToNext()) {
            CardView card = (CardView) getLayoutInflater().inflate(R.layout.steeping_element_layout, relativeLayout, false);
            RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams) card.getLayoutParams();

            final String steepRecipe = cursor.getString(0);
            final long steepStartTime = cursor.getLong(1);
            final long steepDuration = cursor.getLong(2);
            final String steepMeasure = cursor.getString(3);
            float steepAmount = cursor.getFloat(4);
            int factor = 1000;
            if (steepMeasure.equals("days")) {
                factor *= 24 * 60 * 60;
            } else if (steepMeasure.equals("hours")) {
                factor *= 60 * 60;
            }
            long durationInMillis = steepDuration * factor;
            final long endingPoint = steepStartTime + durationInMillis;

            final int cardId = R.string.card_id + cardNumber;
            card.setId(cardId);
            if (cardNumber > 0) {
                relativeParams.addRule(RelativeLayout.BELOW, cardId - 1);
            }
            cardNumber++;

            ImageButton steepBtn = (ImageButton) card.findViewById(R.id.steepingBtn);
            if (endingPoint < System.currentTimeMillis()) {
                steepBtn.setImageResource(R.drawable.ic_check_circle_white_24dp);
            }
            TextView steepingHeader = (TextView) card.findViewById(R.id.steepingTextViewHeader);
            String header = steepRecipe + "     " + steepAmount + " " + getString(R.string.milliliter);
            steepingHeader.setText(header);

            TextView steepingBegan = (TextView) card.findViewById(R.id.steepingTextViewSubHeader);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
            Date startDate = new Date(steepStartTime);

            String beganAt = getString(R.string.steeping_start) + " " + sdf.format(startDate);
            steepingBegan.setText(beganAt);

            TextView steepingContent = (TextView) card.findViewById(R.id.steepingTextViewContent);
            String duration = steepDuration + " ";
            switch (steepMeasure) {
                case "days":
                    duration += getString(R.string.steeping_days);
                    break;
                case "hours":
                    duration += getString(R.string.steeping_hours);
                    break;
                default:
                    duration += "Sekunden";
            }
            steepingContent.setText(duration);

            TextView steepingEnd = (TextView) card.findViewById(R.id.steepingTextViewEnd);
            Date endDate = new Date(endingPoint);

            String endAt = getString(R.string.steeping_end) + " " + sdf.format(endDate);
            steepingEnd.setText(endAt);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Steepings.this);
                    deleteDialog.setMessage(R.string.dialog_clear_steeping_reminder);
                    deleteDialog.setCancelable(true);

                    deleteDialog.setPositiveButton(
                            R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    write.delete(tableNameSteepings, tableSteepingsColumns[0] + "=? AND " + tableSteepingsColumns[1] + "=?",
                                            new String[]{steepRecipe, String.valueOf(steepStartTime)});
                                    if (System.currentTimeMillis() < endingPoint) {
                                        NotificationPublisher.cancelSteeping(steepRecipe, steepStartTime);
                                    }
                                    finish();
                                    Intent reload = getIntent();
                                    startActivity(reload);
                                    dialog.cancel();
                                }
                            }
                    );

                    deleteDialog.setNegativeButton(
                            R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );

                    AlertDialog alert = deleteDialog.create();
                    alert.show();
                }
            };

            card.setOnClickListener(listener);

            card.setLayoutParams(relativeParams);
            if (relativeLayout != null) {
                relativeLayout.addView(card);
            }
        }
        cursor.close();
    }

    private void clearEntries() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Steepings.this);
        deleteDialog.setMessage(R.string.dialog_clear_all_steeping_reminder);
        deleteDialog.setCancelable(true);

        deleteDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        write.delete(tableNameSteepings, null, null);
                        NotificationPublisher.cancelAllSteepings();
                        finish();
                        Intent reload = getIntent();
                        startActivity(reload);
                        dialog.cancel();
                    }
                }
        );

        deleteDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDialog.create();
        alert.show();
    }
}
