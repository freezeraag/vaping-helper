package de.freezeraag.vapinghelper;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class Bases extends AppCompatActivity {

    private SQLiteDatabase write;
    private String tableNameBase;
    private String[] tableBaseColumns;
    private String tableNameBaseInRecipe;
    private String[] tableBaseInRecipeColumns;
    private String tableNameFlavorInRecipe;
    private String[] tableFlavorInRecipeColumns;
    private String tableNameRecipe;
    private String[] tableRecipeColumns;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);

        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        deleteBtn.setEnabled(true).setVisible(true);

        MenuItem addBtn = menu.findItem(R.id.menu_add);
        addBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_add:
                newBase();
                break;
            case R.id.menu_delete:
                clearEntries();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(Bases.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent reload = getIntent();
        startActivity(reload);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bases);
        this.setTitle(getResources().getString(R.string.base_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        final SQLiteDatabase read = db.getReadableDatabase();
        write = db.getWritableDatabase();
        tableNameBase = getResources().getString(R.string.bases_table_name);
        tableBaseColumns = getResources().getStringArray(R.array.bases_table_columns);
        tableNameBaseInRecipe = getResources().getString(R.string.base_in_recipes_table_name);
        tableBaseInRecipeColumns = getResources().getStringArray(R.array.base_in_recipes_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        tableFlavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);
        tableNameRecipe = getResources().getString(R.string.recipes_table_name);
        tableRecipeColumns = getResources().getStringArray(R.array.recipes_table_columns);
        Cursor cursor = read.query(tableNameBase, tableBaseColumns, null, null, null, null,
                tableBaseColumns[0] + " ASC", null);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.myBases);

        while (cursor.moveToNext()) {
            final String baseName = cursor.getString(0);
            final String basePg = String.valueOf(cursor.getFloat(1));
            final String baseVg = String.valueOf(cursor.getFloat(2));
            final String baseWater = String.valueOf(cursor.getFloat(3));
            final String baseNicotine = String.valueOf(cursor.getFloat(4));
            final String basePrice = String.valueOf(cursor.getFloat(5));

            // make a new card
            CardView card = (CardView) getLayoutInflater().inflate(R.layout.list_element_layout, linearLayout, false);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Bases.this, CreateBase.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(tableBaseColumns[0], baseName);
                    bundle.putString(tableBaseColumns[1], basePg);
                    bundle.putString(tableBaseColumns[2], baseVg);
                    bundle.putString(tableBaseColumns[3], baseWater);
                    bundle.putString(tableBaseColumns[4], baseNicotine);
                    bundle.putString(tableBaseColumns[5], basePrice);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            };

            View.OnLongClickListener longClickListener = new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Bases.this);
                    deleteDialog.setMessage(R.string.dialog_clear_base_message);
                    deleteDialog.setCancelable(true);

                    deleteDialog.setPositiveButton(
                            R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // cursor for base_in_recipe_table
                                    final Cursor cursorBaseInRecipe = read.query(tableNameBaseInRecipe, tableBaseInRecipeColumns,
                                            tableBaseInRecipeColumns[1] + "=?",
                                            new String[]{baseName}, null, null, null, null);

                                    if (cursorBaseInRecipe.getCount() > 0) {
                                        dialog.cancel();
                                        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(Bases.this);
                                        String message = getResources().getString(R.string.dialog_clear_base_found_in_recipe1) + " " +
                                                cursorBaseInRecipe.getCount() + " " + getResources().getString(R.string.dialog_clear_base_found_in_recipe2) +
                                                " " + getResources().getString(R.string.dialog_clear_base_found_in_recipe3);
                                        deleteDialog.setMessage(message);
                                        deleteDialog.setCancelable(true);

                                        deleteDialog.setPositiveButton(
                                                R.string.yes,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        while (cursorBaseInRecipe.moveToNext()) {
                                                            write.delete(tableNameFlavorInRecipe, tableFlavorInRecipeColumns[0] + "=?",
                                                                    new String[]{cursorBaseInRecipe.getString(0)});
                                                            write.delete(tableNameRecipe, tableRecipeColumns[0] + "=?",
                                                                    new String[]{cursorBaseInRecipe.getString(0)});
                                                        }
                                                        cursorBaseInRecipe.close();
                                                        write.delete(tableNameBaseInRecipe, tableBaseInRecipeColumns[1] + "=?",
                                                                new String[]{baseName});
                                                        write.delete(tableNameBase, tableBaseColumns[0] + "=?", new String[]{baseName});
                                                        dialog.cancel();
                                                        Toast.makeText(Bases.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                                        finish();
                                                    }
                                                }
                                        );

                                        deleteDialog.setNegativeButton(
                                                R.string.no,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                }
                                        );

                                        AlertDialog alert = deleteDialog.create();
                                        alert.show();
                                    } else {
                                        write.delete(tableNameBase, tableBaseColumns[0] + "=?", new String[]{baseName});
                                        dialog.cancel();
                                        Toast.makeText(Bases.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            }
                    );

                    deleteDialog.setNegativeButton(
                            R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    );

                    AlertDialog alert = deleteDialog.create();
                    alert.show();
                    return true;
                }
            };


            Button aBase = (Button) card.findViewById(R.id.list_element_btn);
            String btnText = baseName.charAt(0) + "";
            aBase.setText(btnText);
            aBase.setOnClickListener(listener);
            aBase.setOnLongClickListener(longClickListener);

            TextView textView = (TextView) card.findViewById(R.id.list_element_textView);
            String header = baseName + "  -  " + baseNicotine + " " + getString(R.string.milligrammPerMilliliter) + "\n" +
                    basePg + "/" + baseVg + "/" + baseWater;
            textView.setText(header);
            card.setOnClickListener(listener);
            card.setOnLongClickListener(longClickListener);

            if (linearLayout != null) {
                linearLayout.addView(card);
            }
        }
        cursor.close();
    }

    public void newBase() {
        Intent openNewBase = new Intent(this, CreateBase.class);
        startActivity(openNewBase);
    }

    public void clearEntries() {
        AlertDialog.Builder deleteDbDialog = new AlertDialog.Builder(Bases.this);
        deleteDbDialog.setMessage(R.string.dialog_clear_table_bases_message);
        deleteDbDialog.setCancelable(true);

        deleteDbDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        write.delete(tableNameBase, null, null);
                        write.delete(getString(R.string.base_in_recipes_table_name), null, null);
                        write.delete(getString(R.string.flavor_in_recipes_table_name), null, null);
                        write.delete(getString(R.string.recipes_table_name), null, null);
                        Intent reload = getIntent();
                        startActivity(reload);
                        dialog.cancel();
                        finish();
                    }
                }
        );

        deleteDbDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDbDialog.create();
        alert.show();
    }
}
