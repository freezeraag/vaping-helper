package de.freezeraag.vapinghelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_settings:
                Intent settings = new Intent(MainActivity.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        final Intent openMix = new Intent(this, Mix.class);
        Button mixBtn = (Button) findViewById(R.id.btn_mix);
        if (mixBtn != null) {
            mixBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(openMix);
                }
            });
        }

        final Intent openBases = new Intent(this, Bases.class);
        Button basesBtn = (Button) findViewById(R.id.btn_bases);
        if (basesBtn != null) {
            basesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(openBases);
                }
            });
        }

        final Intent openFlavors = new Intent(this, Flavors.class);
        Button flavorsBtn = (Button) findViewById(R.id.btn_flavors);
        if (flavorsBtn != null) {
            flavorsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(openFlavors);
                }
            });
        }

        final Intent openRecipes = new Intent(this, Recipes.class);
        Button recipesBtn = (Button) findViewById(R.id.btn_recipes);
        if (recipesBtn != null) {
            recipesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(openRecipes);
                }
            });
        }

        final Intent openSteepings = new Intent(this, Steepings.class);
        Button steepingsBtn = (Button) findViewById(R.id.btn_steepings);
        if (steepingsBtn != null) {
            steepingsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(openSteepings);
                }

            });
        }
    }
}
