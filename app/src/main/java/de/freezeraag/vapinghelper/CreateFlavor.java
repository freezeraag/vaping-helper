package de.freezeraag.vapinghelper;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import de.freezeraag.vapinghelper.db.VapingHelperDatabaseOpenHelper;

public class CreateFlavor extends AppCompatActivity {

    private SQLiteDatabase read;
    private SQLiteDatabase write;
    private ContentValues cv;
    private String tableNameFlavor;
    private String[] tableFlavorColumns;
    private String tableNameFlavorInRecipe;
    private String[] tableFlavorInRecipeColumns;

    private EditText manufacturer;
    private String manufacturerString;
    private EditText flavorName;
    private String flavorString;
    private EditText flavorPrice;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.standard_menu, menu);
        MenuItem deleteBtn = menu.findItem(R.id.menu_delete);
        if (getIntent().hasExtra(tableFlavorColumns[0])) {
            deleteBtn.setEnabled(true).setVisible(true);
        } else {
            deleteBtn.setEnabled(false).setVisible(false);
        }

        MenuItem saveBtn = menu.findItem(R.id.menu_save);
        saveBtn.setEnabled(true).setVisible(true);

        MenuItem settingsBtn = menu.findItem(R.id.menu_settings);
        settingsBtn.setEnabled(true).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_save:
                saveFlavor();
                break;
            case R.id.menu_delete:
                deleteFlavor();
                break;
            case R.id.menu_settings:
                Intent settings = new Intent(CreateFlavor.this, Settings.class);
                startActivity(settings);
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_flavor);
        this.setTitle(getResources().getString(R.string.create_flavor_title));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }

        VapingHelperDatabaseOpenHelper db = new VapingHelperDatabaseOpenHelper(this);
        read = db.getWritableDatabase();
        write = db.getWritableDatabase();
        cv = new ContentValues();
        tableNameFlavor = getResources().getString(R.string.flavors_table_name);
        tableFlavorColumns = getResources().getStringArray(R.array.flavors_table_columns);
        tableNameFlavorInRecipe = getResources().getString(R.string.flavor_in_recipes_table_name);
        tableFlavorInRecipeColumns = getResources().getStringArray(R.array.flavor_in_recipes_table_columns);

        manufacturer = (EditText) findViewById(R.id.newManufacturer);
        flavorName = (EditText) findViewById(R.id.newFlavor);
        flavorPrice = (EditText) findViewById(R.id.newPrice);

        if (getIntent().hasExtra(tableFlavorColumns[0])) {
            manufacturer.setText(getIntent().getStringExtra(tableFlavorColumns[0]));
            manufacturerString = getIntent().getStringExtra(tableFlavorColumns[0]);
            flavorName.setText(getIntent().getStringExtra(tableFlavorColumns[1]));
            flavorString = getIntent().getStringExtra(tableFlavorColumns[1]);
            flavorPrice.setText(getIntent().getStringExtra(tableFlavorColumns[2]));
        } else {
            manufacturerString = "";
            flavorString = "";
        }
    }

    private void saveFlavor() {
        String actualManufaturer = manufacturer.getText().toString().trim();
        String actualFlavor = flavorName.getText().toString().trim();
        String actualPrice = flavorPrice.getText().toString().trim();
        if (!actualManufaturer.equals("") && !actualFlavor.equals("")) {
            cv.put(tableFlavorColumns[0], actualManufaturer);
            cv.put(tableFlavorColumns[1], actualFlavor);
            cv.put(tableFlavorColumns[2], actualPrice);
            if (getIntent().hasExtra(tableFlavorColumns[0])) {
                write.execSQL("DELETE FROM " + tableNameFlavor + " WHERE " + tableFlavorColumns[0] +
                        " = '" + manufacturerString + "' AND " + tableFlavorColumns[1] +
                        "= '" + flavorString + "'");
            }
            write.insertWithOnConflict(tableNameFlavor, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
            Toast.makeText(CreateFlavor.this, R.string.toast_saved, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(CreateFlavor.this, R.string.toast_empty_fields, Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteFlavor() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(CreateFlavor.this);
        deleteDialog.setMessage(R.string.dialog_clear_flavor_message);
        deleteDialog.setCancelable(true);

        deleteDialog.setPositiveButton(
                R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // cursor for flavor_in_recipe_table
                        final Cursor cursorFlavorInRecipe = read.query(tableNameFlavorInRecipe,
                                tableFlavorInRecipeColumns,
                                tableFlavorInRecipeColumns[1] + "=? AND " +
                                        tableFlavorInRecipeColumns[2] + "=?",
                                new String[]{manufacturerString, flavorString}, null, null, null, null);

                        if (cursorFlavorInRecipe.getCount() > 0) {
                            dialog.cancel();
                            AlertDialog.Builder deleteDialog = new AlertDialog.Builder(CreateFlavor.this);
                            String message = getResources().getString(R.string.dialog_clear_flavor_found_in_recipe1) + " " +
                                    cursorFlavorInRecipe.getCount() + " " + getResources().getString(R.string.dialog_clear_flavor_found_in_recipe2) +
                                    " " + getResources().getString(R.string.dialog_clear_flavor_found_in_recipe3);
                            deleteDialog.setMessage(message);
                            deleteDialog.setCancelable(true);

                            deleteDialog.setPositiveButton(
                                    R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            cursorFlavorInRecipe.close();
                                            write.delete(tableNameFlavorInRecipe,
                                                    tableFlavorInRecipeColumns[1] + "=? AND " +
                                                            tableFlavorInRecipeColumns[2] + "=?",
                                                    new String[]{manufacturerString, flavorString});
                                            write.delete(tableNameFlavor, tableFlavorColumns[0] +
                                                            "=? AND " + tableFlavorColumns[1] + "=?",
                                                    new String[]{manufacturerString, flavorString});
                                            dialog.cancel();
                                            Toast.makeText(CreateFlavor.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                            finish();
                                        }
                                    }
                            );

                            deleteDialog.setNegativeButton(
                                    R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }
                            );

                            AlertDialog alert = deleteDialog.create();
                            alert.show();
                        } else {
                            write.delete(tableNameFlavor, tableFlavorColumns[0] +
                                            "=? AND " + tableFlavorColumns[1] + "=?",
                                    new String[]{manufacturerString, flavorString});
                            dialog.cancel();
                            Toast.makeText(CreateFlavor.this, R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
        );

        deleteDialog.setNegativeButton(
                R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog alert = deleteDialog.create();
        alert.show();
    }
}